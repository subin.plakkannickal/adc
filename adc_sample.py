import smbus
import time

class I2C():
	def __init__(self):
		''' 
		This module following I2C protocol. Digital to analog cancerter and analog to digital converater are included.
		'''
		self.bus = smbus.SMBus(1)
		self.address = 0x48
		self.digitalOut = 0x00

	def DAC(self, digitalInput):
		'''
		para: digitalInput: int, 16-bit, 
		return: None (Analog output from PCF 8591T)
		'''
		time.sleep(.001)
		self.bus.write_byte_data(self.address, 0x41, digitalInput)

	def ADC(self,channel):
		'''
		para: channel: int, 2-bit
		return: digitalOut, int, 16-bit
		'''
		if channel == 0:
			time.sleep(.001)
			return self.bus.read_byte_data(self.address, 0x00)
		elif channel == 1:
                        time.sleep(.001)
                        return self.bus.read_byte_data(self.address, 0x01)
		elif channel == 2:
                        time.sleep(.001)
                        return self.bus.read_byte_data(self.address, 0x02)
		elif channel == 3:
                        time.sleep(.001)
                        return self.bus.read_byte_data(self.address, 0x03)
